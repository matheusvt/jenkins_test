pipeline {
    agent any
    
     environment {
        PATH="/home/bitnami/miniconda3/bin:$PATH"
    }
    
    stages {
        stage('Test') {
            steps {
                sh 'dir'
                println "Testing..."
                sh 'whoami'
            }
        }    
         stage('Python') {
            steps {
                sh 'dir'
                println "Virtualenv..."
            }
        }
    

        stage('Unit tests') {
            steps {
            sh '''
                #conda create --yes -n ${BUILD_TAG} python
                #source activate ${BUILD_TAG}
                #pip install flask django
            '''
            }
        }
        
        stage('Test environment') {
            parallel{
                    stage('In Parallel 1') {
                        steps {
                            sh '''#source activate ${BUILD_TAG} 
                                #pip list
                                #which pip
                                #which python
                                #which git
                                '''
                            }
                        }
                    stage('In Parallel 2') {
                        steps {
                            dir ('docker') {
                                checkout scm
                                sh "ls -lat"
                                sh 'dir'        
                                sh 'docker build -t ${BUILD_TAG}:${BUILD_ID}  .'
                                sh 'docker images'
                            }
                            
                            }
                        }    
                    }    
                }
            }
        
    post {
        always {
            sh 'conda remove --yes -n ${BUILD_TAG} --all'
            sh 'docker rmi ${BUILD_TAG}:${BUILD_ID}'
            sh 'docker images'
            
        }
    }
}